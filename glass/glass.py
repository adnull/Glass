import time
import asyncio
import logging

from .client import Client
from .exceptions import StreamOffline

log = logging.getLogger(__name__)

class Glass:
	def __init__(self):
		self.clients = []
		self.loop = asyncio.get_event_loop()
		self.running = False

	def set_accounts(self, accounts=[]):
		for account in accounts:
			self.clients.append(Client(account))

	def add_channel(self, channel_name):
		set = False

		while not set:
			try:
				for client in self.clients:
					client.add_channel(channel_name)

				set = True
			except StreamOffline:
				log.warning("Streamer is offline.")
				self.wait_for(channel_name)

	def set_fallback(self, channels):
		"""
		Add channels to watch if the main channel goes offline. As soon as the
		originally set channel comes back online, we watch that instead.
		"""
		pass

	def check_live(self, channel_name):
		# Todo: implement logged out check, should be easy
		# Todo: make a channel class to manage all of this easily
		for channel in self.clients[0].channels:
			if channel.channel_name == channel_name:
				return self.clients[0].gql.is_live(channel.channel_id)
		raise Exception("Invalid channel")

	def wait_for(self, channel_name):
		"""
		Wait for a specific streamer to come online
		"""
		while not self.check_live(channel_name):
			log.debug(f"Waiting for @{channel_name} to start streaming")
			time.sleep(60)

	def run(self):
		"""
		Run the bot across all clients
		"""
		self.wait_for(self.clients[0].channel_name)

		tasks = []

		for client in self.clients:
			for task in client.run():
				tasks.append(task)

		log.info(f"Started the bot across {str(len(self.clients))} account(s)")
		self.loop.run_until_complete(asyncio.gather(*tasks))
