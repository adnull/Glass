import logging
import websockets

from .websocket import Websocket

log = logging.getLogger(__name__)

"""
The IRC class is no longer needed as you will still get points if you aren't
connected to the IRC socket.
"""

class Irc:
	def __init__(self):
		"""
		The websocket for the IRC connection. Chat messages and some channel
		updates will be sent to this socket.
		"""

		# Websocket variables
		self.ws = 0
		self.active = False
		self.initialized = False

		# User data
		self.username = ''
		self.auth = ''

		# Channel data
		self.channel_name = ''

	def user(self, username, auth):
		"""
		Initialize the user's auth and username

		username	-> username of the Twitch account
		auth		-> user's auth token
		"""
		self.username = username
		self.auth = auth

	def set_channel(self, new_channel):
		"""
		Update the current channel

		new_channel	-> name of the new channel to join
		"""

		# Leave the old channel and join the new one
		if self.active:
			self.ws.send(f"PART #{self.channel_name}")
			self.ws.send(f"JOIN #{new_channel}")

		self.channel_name = new_channel

	async def initialize(self):
		await self.ws.send("CAP REQ :twitch.tv/tags twitch.tv/commands")
		await self.ws.send(f"PASS oauth:{self.auth}")
		await self.ws.send(f"NICK {self.username}")
		await self.ws.send(f"USER {self.username} 8 * :{self.username}")
		await self.ws.send(f"JOIN #{self.channel_name}")

	async def run(self):
		self.ws = await Websocket().__aenter__("wss://irc-ws.chat.twitch.tv/")
		self.active = True

		while self.active:
			try:
				if not self.initialized:
					await self.initialize()

				while self.active:
					data = await self.ws.recv()

					if data.startswith("PING"):
						await self.ws.send("PONG")
			except websockets.exceptions.ConnectionClosed:
				log.error("Websocket closed unexpectedly.")
				self.initialized = False
				await self.ws.__aexit__(
					exc_type=None,
					exc_value=None,
					traceback=None
				)
