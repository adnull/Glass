import json
import requests
import logging
import urllib.parse

from .gql import GraphQL
from .channel import Channel
from .websocket.pubsub import PubSub

log = logging.getLogger(__name__)

class Client:
	def __init__(self, cookies=[]):
		# Currently watching data
		self.channel_name = ''

		# Initialize the session
		self.session = requests.Session()
		self.session.headers.update({
			"User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:80.0) Gecko/20100101 Firefox/80.0"
		})

		self.set_cookies(cookies)

		self.gql = GraphQL(self.session)

		# User data
		self.username = ''
		self.auth = ''
		self.unique_id = ''
		self.user_id = ''
		self.client_id = ''
		
		self.initialize_user()

		# Channel data
		self.channels = []

		# Websockets
		self.pubsub = PubSub(self.gql)

	def initialize_user(self):
		"""
		Initialize all of the user data we can find within the `twilight-user`
		cookie including username, auth (token), unique_id, and user_id.
		"""
		user_data = json.loads(urllib.parse.unquote(self.session.cookies.get("twilight-user")))

		self.username = user_data["displayName"]
		self.auth = user_data["authToken"]
		self.unique_id = self.session.cookies["unique_id"]
		self.user_id = user_data["id"]

	def set_cookies(self, cookies):
		"""
		Set all of the cookies from a dictionary. Each cookie must have a `name`
		and a `value` key.
		cookies 	-> array of valid cookies
		"""
		for cookie in cookies:
			cookie_obj = requests.cookies.create_cookie(
				domain=cookie["domain"],
				name=cookie["name"],
				value=cookie["value"]
			)

			self.session.cookies.set_cookie(cookie_obj)

	def add_channel(self, new_channel):
		self.channel_name = new_channel

		resp = self.session.get(f"https://www.twitch.tv/{new_channel}")
		self.client_id = resp.text.split('"Client-ID":"')[-1].split('"')[0]

		channel = Channel(self.session, self.gql, self.pubsub)
		
		channel.set_user(self.username, self.auth, self.unique_id, self.user_id, self.client_id)
		channel.set_channel(new_channel)

		self.channels.append(channel)

	def deactivate(self):
		"""
		Stop all processes that the client has open from running, namely the
		websockets and the viewer.
		"""
		self.pubsub.active = False

		for channel in self.channels:
			channel.deactivate()

	def run(self):
		self.pubsub.user(self.username, self.user_id, self.auth)

		tasks = [self.pubsub.run()]

		for channel in self.channels:
			tasks.append(channel.watch())

		return tasks
