class StreamerException(Exception):
	pass

class StreamOffline(StreamerException):
	pass

class InvalidStreamer(StreamerException):
	pass
