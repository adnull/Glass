import ssl
import websockets

class Websocket:
	async def __aenter__(self, url):
		ssl_context = ssl.SSLContext()
		ssl_context.check_hostname = False
		ssl_context.verify_mode = ssl.CERT_NONE

		self._conn = websockets.connect(
			url,
			ssl=ssl_context,
			max_size=10 * 1024 * 1024
		)
		self.ws = await self._conn.__aenter__()

		return self

	async def __aexit__(self, *args, **kwargs):
		await self._conn.__aexit__(*args, **kwargs)

	async def send(self, data):
		await self.ws.send(data)

	async def recv(self):
		return await self.ws.recv()
