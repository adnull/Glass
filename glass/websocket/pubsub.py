import json
import time
import asyncio
import logging
import websockets

from .. import utilities
from .websocket import Websocket

log = logging.getLogger(__name__)

class PubSub:
	def __init__(self, gql):
		"""
		Within this websocket we will receive all updates regarding 'topics'
		that we 'subscribe' to. In this instance we focus mainly on updates
		to points, stream updates (offline), raids, and hosts.
		"""
		self.gql = gql

		# Websocket variables
		self.active = False
		self.ws = 0
		self.initialized = False

		# User data
		self.username = ''
		self.user_id = ''
		self.channel_id = ''
		self.auth = ''

	def user(self, username, user_id, auth):
		"""
		Initialize the user's authentication variables

		user_id -> Twilight user's `user_id`
		auth 	-> Twilight user's `authToken`
		"""
		self.username = username
		self.user_id = user_id
		self.auth = auth

	def set_channel(self, channel_id):
		"""
		Update the channel ID. Also accounts for if we are in a stream
		currently.

		channel_id 	-> the ID of the new channel from its token.
		"""
		self.channel_id = channel_id

		# Make sure to update data/reinitialize with new channel ID
		if self.active:
			pass

	async def initialize(self):
		"""
		Start following all of the necessary topics.
		"""
		await self.ping()

		await self.listen("stream-change-v1", self.user_id, self.auth)
		await self.listen("user-drop-events", self.user_id, self.auth)
		await self.listen("user-preferences-update-v1", self.user_id, self.auth)
		await self.listen("community-points-user-v1", self.user_id, self.auth)
		await self.listen("community-points-channel-v1", self.channel_id, self.auth)
		await self.listen("follows", self.user_id, self.auth)
		await self.listen("user-properties-update", self.user_id, self.auth)
		await self.listen("user-subscribe-events-v1", self.user_id, self.auth)
		await self.listen("presence", self.user_id, self.auth)
		await self.listen("ads", self.channel_id, self.auth)
		await self.listen("pv-watch-party-events", self.channel_id, self.auth)
		await self.listen("extension-control", self.channel_id, self.auth)
		await self.listen("broadcast-settings-update", self.channel_id, self.auth)
		await self.listen("chatrooms-user-v1", self.user_id, self.auth)
		await self.listen(f"chat_moderator_actions.{str(self.user_id)}", self.channel_id, self.auth)
		await self.listen("stream-chat-room-v1", self.channel_id, self.auth)
		await self.listen("polls", self.channel_id, self.auth)
		await self.listen("user-bits-updates-v1", self.user_id, self.auth)
		await self.listen("video-playback-by-id", self.channel_id, self.auth)

	async def ping(self):
		"""
		Send a ping to the server. We will receive a `PONG` once the server
		has received our `PING`.
		"""
		await self.ws.send(json.dumps({
			"type": "PING"
		}))

	async def listen(self, topic, target_id, auth):
		"""
		Set up and send a new `LISTEN` request. This will start following the
		specified topic.

		topic 		-> the topic to follow
		target_id 	-> channel/user ID to follow
		auth 		-> authentication token
		"""
		await self.ws.send(json.dumps({
			"type": "LISTEN",
			"nonce": utilities.nonce(30), # Nonces can be random
			"data": {
				"topics": [f"{topic}.{str(target_id)}"],
				"auth_token": auth
			}
		}))

	def process_message(self, data):
		"""
		Process the contents of a message, currently just used to check for
		point drops or claims and log them.
		"""
		if data["type"] == "MESSAGE":
			message = json.loads(data["data"]["message"])

			if not message.get("type"):
				return

			if message["type"] == "claim-available":
				self.gql.claim(message["data"]["claim"]["channel_id"], message["data"]["claim"]["id"])
			elif message["type"] == "points-earned":
				log.info(f"<{message['data']['channel_id']}> <{self.username}> Earned {str(message['data']['point_gain']['total_points'])} points! ({str(message['data']['balance']['balance'])} total)")

	async def run(self):
		"""
		Run the websocket. A ping must be sent to the server every 4 minutes,
		otherwise the connection will die.
		"""
		self.ws = await Websocket().__aenter__("wss://pubsub-edge.twitch.tv/v1")
		self.active = True
		
		while self.active:
			try:
				if not self.initialized:
					await self.initialize()

				last_ping = time.time()

				while self.active:
					try:
						raw_data = await asyncio.wait_for(self.ws.recv(), timeout=10)
					except asyncio.TimeoutError:
						if last_ping + (4 * 60) < time.time():
							await self.ping()
							last_ping = time.time()
						continue
					self.process_message(json.loads(raw_data))
			except websockets.exceptions.ConnectionClosed:
				log.error("Websocket closed unexpectedly.")
				self.initialized = False
				await self.ws.__aexit__(
					exc_type=None,
					exc_value=None,
					traceback=None
				)
