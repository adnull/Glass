import json
import time
import m3u8
import base64
import random
import asyncio
import logging

from . import utilities
from .exceptions import StreamOffline, InvalidStreamer

log = logging.getLogger(__name__)

class Viewer:
	def __init__(self, session, gql):
		self.session = session
		self.gql = gql

		self.active = True

		self.username = ''
		self.client_id = ''
		self.user_id = ''

		self.channel_name = ''
		self.channel_id = ''

		self.watch_time = 0
		self.minutes = 0
		self.twilight_version = ''
		self.serving_id = ''

		self.app_session_id = utilities.hash(16)
		self.tab_session_id = utilities.hash(16)
		self.play_session_id = utilities.hash(16)
		self.localstorage_device_id = utilities.hash(16)
		self.page_session_id = utilities.hash(16)
		self.benchmark_session_id = utilities.hash(16)

	def user(self, username, client_id, user_id):
		self.username = username
		self.client_id = client_id
		self.user_id = user_id

	def set_channel(self, channel_name, channel_id):
		self.channel_name = channel_name
		self.channel_id = channel_id
		self.broadcast_id = self.gql.broadcast_id(self.channel_name)

	def extra_data(self, twilight_version=None, spade_url=None):
		if twilight_version:
			self.twilight_version = twilight_version

		if spade_url:
			self.spade_url = spade_url

	def mark_watched(self, playlist_data):
		self.minutes += 1

		now = time.time()

		cluster = playlist_data.split('CLUSTER="')[-1].split('"')[0]
		manifest_cluster = playlist_data.split('MANIFEST-CLUSTER="')[-1].split('"')[0]
		manifest_node = playlist_data.split('MANIFEST-NODE="')[-1].split('"')[0]
		manifest_node_type = playlist_data.split('MANIFEST-NODE-TYPE="')[-1].split('"')[0]
		node = playlist_data.split('NODE="')[-1].split('"')[0]

		# Todo but not urgent, check which of the values in `data` make us gain
		# a legitimate minute watched, and remove those that aren't necessary.
		self.session.post(self.spade_url, headers={
			"Accept": "*/*",
			"Accept-Language": "en-US,en;q=0.5",
			"Accept-Encoding": "gzip, deflate, br",
			"Content-Type": "application/x-www-form-urlencoded; charset=utf-8",
			"Origin": "https://www.twitch.tv",
			"Referer": f"https://www.twitch.tv/{self.channel_name}",
		}, data={
			"data": base64.b64encode(json.dumps([
				{
					"event": "minute-watched",
					"properties": {
						"app_session_id": self.app_session_id,
						"app_version": self.twilight_version,
						"device_id": self.session.cookies["unique_id"],
						"domain": "twitch.tv",
						"host": "www.twitch.tv",
						"platform": "web",
						"preferred_language": "en-US",
						"referrer_host": "www.twitch.tv",
						"referrer_url": "",
						"received_language": "en",
						"tab_session_id": self.tab_session_id,
						"batch_time": int(now),
						"url": "https://www.twitch.tv/" + self.channel_name,
						"client_time": str(now)[:-4],
						"bornuser": False,
						"browser": "5.0 (Windows)",
						"browser_family": "firefox",
						"browser_version": "80.0",
						"collapse_right": False,
						"collapse_left": True,
						"localstorage_device_id": self.localstorage_device_id,
						"location": "channel",
						"page_session_id": self.page_session_id,
						"referrer": "https://www.twitch.tv/" + self.channel_name,
						"referrer_domain": "www.twitch.tv",
						"session_device_id": self.session.cookies["unique_id"],
						"benchmark_server_id": self.session.cookies["server_session_id"],
						"viewport_height": 966,
						"viewport_width": 905,
						"channel": self.channel_name,
						"channel_id": str(self.channel_id),
						"game": "Among Us", # this doesn't really seem to affect points?
						"hosted_game": None,
						"is_following": False,
						"is_live": True,
						"language": "en",
						"average_bitrate": 677400,
						"backend": "mediaplayer",
						"broadcast_id": self.broadcast_id,
						"cluster": cluster,
						"core_version": "1.0.1-c71ad1ac",
						"current_bitrate": random.randint(500000, 750000),
						"current_fps": random.randint(28, 32),
						"decoded_frames": self.watch_time * 30,
						"dropped_frames": 0,
						"estimated_bandwidth": (random.randint(5, 50) * 1024 * 1024) / 8,
						"frame_variance": random.randint(950, 1050),
						"hidden": False,
						"hls_latency_broadcaster": random.randint(5900, 6100),
						"hls_latency_ingest": random.randint(5900, 6100),
						"live": True,
						"low_latency": True,
						"manifest_cluster": manifest_cluster,
						"manifest_node": manifest_node,
						"manifest_node_type": manifest_node_type,
						"minutes_logged": self.minutes,
						"muted": False,
						"node": node,
						"origin_dc": "sjc02",
						"os_name": "Windows",
						"os_version": "NT 10.0",
						"play_session_id": self.play_session_id,
						"playback_rate": 1,
						"player": "site",
						"quality": "auto",
						"rendered_frames": self.watch_time * 30,
						"seconds_offset": random.randint(59, 61),
						"serving_id": self.serving_id,
						"stream_format": "360p30",
						"time": str(now)[:-4],
						"transcoder_type": "2017TranscodeX264_V2",
						"transport_download_bytes": 5078068, # May need to edit these?
						"transport_download_duration": 43621,
						"transport_first_byte_latency": 8528,
						"transport_segment_duration": 59972,
						"transport_segments": 30,
						"user_agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:80.0) Gecko/20100101 Firefox/80.0",
						"vid_height": 360,
						"vid_width": 640,
						"video_buffer_size": random.uniform(4.900000, 5.100000),
						"volume": 0.5,
						"is_pbyp": False,
						"squad_stream_id": None,
						"squad_stream_session_id": None,
						"squad_stream_presentation_id": None,
						"is_mod": False,
						"time_spent_hidden": 0,
						"consent_comscore_ok": True,
						"app_fullscreen": False,
						"autoplayed": True,
						"backend_version": "1.0.1-c71ad1ac", # May need to change also
						"broadcaster_software": "unknown_rtmp",
						"captions_enabled": False,
						"chat_visible": True,
						"chat_visibility_status": "visible",
						"content_mode": "live",
						"host_channel": None,
						"host_channel_id": None,
						"is_ad_playing": False,
						"logged_in": True,
						"login": self.username,
						"mse_support": True,
						"partner": True,
						"playback_gated": False,
						"player_size_mode": "default",
						"staff": None,
						"subscriber": False,
						"turbo": False,
						"user_id": self.user_id,
						"viewer_exemption_reason": None,
						"subscription_upsell_shown": False,
						"benchmark_session_id": self.benchmark_session_id,
						"client_build_id": self.twilight_version,
						"distinct_id": self.session.cookies["unique_id"],
						"client_app": "twilight"
					}
				}
			]).encode("utf-8"))
		}).raise_for_status()

	async def watch(self, stream):
		"""
		Watch the given stream name until `active` is set to False. We don't
		actually need to make a request for every single .ts file that the
		server sends, we can just get the m3u8 file and then send an 'edge'
		request to the server every minute telling it that we've been watching
		the whole time.
		"""

		# Get the channel's `token` and `sig`(nature)
		resp = self.session.get(f"https://api.twitch.tv/api/channels/{stream}/access_token", params={
			"client_id": self.client_id
		}, headers={
			"Accept": "application/vnd.twitchtv.v5+json",
			"Origin": "https://www.twitch.tv",
			"Referer": f"https://www.twitch.tv/{stream}",
			"X-Requested-With": "XMLHttpRequest"
		})
		data = resp.json()

		resp = self.session.get(f"https://usher.ttvnw.net/api/channel/hls/{stream}.m3u8", params={
			"allow_source": True,
			"fast_bread": True,
			"p": random.randint(1000000, 9999999),
			"client_id": self.client_id,
			"player_backend": "mediaplayer",
			"playlist_include_framerate": True,
			"reassignments_supported": True,
			"sig": data["sig"],
			"supported_codecs": "vp09,avc1",
			"token": data["token"],
			"cdm": "wv",
			"player_version": "1.0.1"
		}, headers={
			"Origin": "https://www.twitch.tv",
			"Referer": f"https://www.twitch.tv/{stream}"
		})

		if "Can not find channel" in resp.text:
			raise InvalidStreamer("Streamer does not exist.")

		self.serving_id = resp.text.split('SERVING-ID="')[-1].split('"')[0]
		
		playlists = m3u8.loads(resp.text).playlists
		log.debug(f"Retrieved {str(len(playlists))} resolutions for @{stream}'s stream")

		if len(playlists) < 1:
			raise StreamOffline("No transcodes exist for the given stream")

		last_minute = time.time()

		while self.active:
			now = time.time()

			if last_minute < now - 60:
				self.watch_time += 60
				last_minute = now

				try:
					self.mark_watched(resp.text)
				except Exception as e:
					log.warning(str(e))

			await asyncio.sleep(30)
