import logging

from .constants import HASHES
from .exceptions import StreamOffline

log = logging.getLogger(__name__)

class GraphQL:
	def __init__(self, session, authenticated=True):
		"""
		Anything to do with Twitch's GraphQL server

		session -> session to use
		"""

		# Actual requests session
		self.session = session
		self.authenticated = authenticated

		# User specific
		self.unique_id = ''
		self.client_id = ''
		self.auth = ''
		self.channel_name = ''

	def user(self, unique_id, client_id, auth):
		self.unique_id = unique_id
		self.client_id = client_id
		self.auth = auth

	def set_channel(self, channel_name):
		self.channel_name = channel_name

	def request(self, data):
		for item in data:
			item["extensions"] = {
				"persistedQuery": {
					"sha256Hash": HASHES[item["operationName"]],
					"version": 1
				}
			}

		resp = self.session.post("https://gql.twitch.tv/gql#origin=twilight", json=data, headers={
			"Authorization": "OAuth " + self.auth,
			"Client-Id": self.client_id,
			"Content-Type": "text/plain;charset=UTF-8",
			"Origin": "https://www.twitch.tv",
			"Referer": f"https://www.twitch.tv/{self.channel_name}",
			"X-Device-Id": self.unique_id
		})
		resp.raise_for_status()

		return resp.json()

	def is_following(self, channel):
		return self.request([
			{
				"operationName": "ChatRestrictions",
				"variables": {
					"channelLogin": channel
				}
			}
		])[0]["data"]["channel"]["self"]["follower"] != None

	def follow(self, channel_id):
		return self.request([
			{
				"operationName": "FollowButton_FollowUser",
				"variables": {
					"input": {
						"disableNotifications": False,
						"targetID": str(channel_id)
					}
				}
			}
		])

	def is_live(self, channel_id):
		return self.request([
			{
				"operationName": "WithIsStreamLiveQuery",
				"variables": {
					"id": str(channel_id)
				}
			}
		])[0]["data"]["user"]["stream"] != None

	def has_available(self, channel):
		"""
		Check the point context for a specific channel to see if we can claim
		50 points/the bonus 'chest' from a previous run.

		channel -> name of the channel to check
		"""
		return self.request([
			{
				"operationName": "ChannelPointsContext",
				"variables": {
					"channelLogin": channel
				}
			}
		])[0]["data"]["community"]["channel"]["self"]["communityPoints"]

	def claim(self, channel_id, claim_id):
		"""
		Claim a point code received from the PubSub socket.

		channel_id	-> ID of the channel to claim points for
		claim_id	-> ID of the points to claim
		"""
		self.request([{
			"operationName": "ClaimCommunityPoints",
			"variables": {
				"input": {
					"channelID": str(channel_id),
					"claimID": str(claim_id)
				}
			}
		}])

	def broadcast_id(self, channel):
		resp = self.request([{
			"operationName": "UseLive",
			"variables": {
				"channelLogin": self.channel_name
			}
		}])

		stream = resp[0]["data"]["user"]["stream"]

		if not stream:
			raise StreamOffline()

		return stream["id"]