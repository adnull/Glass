import os
import pickle
import logging
import argparse

log = logging.getLogger(__name__)

from glass import Glass

def main():
	parser = argparse.ArgumentParser(description="Twitch channel point autoclaimer")

	parser.add_argument("-c", "--cookies", type=str, default="cookies", help="Which folder to get cookies from")
	parser.add_argument("-d", "--debug", action="store_true", default=False, help="Print debug information to the console")
	parser.add_argument("channels", nargs=argparse.REMAINDER, help="What channels to watch")

	args, unk = parser.parse_known_args()

	logging.basicConfig(
		format="[%(asctime)s] [%(name)s] %(levelname)s: %(message)s",
		datefmt="%Y-%m-%d %H:%M:%S",
		level=logging.DEBUG if args.debug else logging.INFO
	)

	accounts = []

	if not os.path.isdir(args.cookies):
		log.fatal("Cookie directory does not exist. Please enter a valid folder name.")
		return

	for file in os.listdir(args.cookies):
		if not file.endswith(".pkl"):
			continue
			
		with open(args.cookies + "/" + file, "rb") as file:
			data = pickle.load(file)

			if not isinstance(data, list):
				log.warning(f"Account file '{file}' does not hold the correct information.")
				continue

			accounts.append(data)

	if len(accounts) < 1:
		return log.fatal("No accounts were detected.")

	client = Glass()
	client.set_accounts(accounts)

	for channel in args.channels:
		client.add_channel(channel)

	client.run()

if __name__ == "__main__":
	main()
