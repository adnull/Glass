
import re
import json
import time
import base64
import random
import logging

from .viewer import Viewer
from .constants import RE_SPADE, RE_CDN

log = logging.getLogger(__name__)

class Channel:
	def __init__(self, session, gql, pubsub):
		self.session = session
		self.gql = gql
		self.pubsub = pubsub

		self.viewer = Viewer(session, gql)

		# CDN session data
		self.spade_url = ''
		self.twilight_version = ''

		self.channel_name = ''
		self.channel_id = ''

		self.username = ''
		self.auth = ''
		self.unique_id = ''
		self.user_id = ''
		self.client_id = ''

	def initialize_cdn(self, streamer_page):
		"""
		Emulate requests made to the CDN for fonts, JS, CSS, and some PNG
		files.

		streamer_page -> response text for the streamer's page
		"""

		# Find the Twilight build ID for edge requests
		self.twilight_version = streamer_page.split('window.__twilightBuildID="')[-1].split('"')[0]

		urls = re.findall(RE_CDN, streamer_page)

		for url in urls:
			if not "settings" in url:
				continue

			url = url.rstrip('"')

			resp = self.session.get(url, headers={
				"Origin": "https://www.twitch.tv",
				"Referer": f"https://www.twitch.tv/{self.channel_name}",
			})

			spade_re = re.search(RE_SPADE, resp.text)
			self.spade_url = spade_re.group(1)
		
		self.viewer.extra_data(
			twilight_version=self.twilight_version,
			spade_url=self.spade_url
		)

	def initialize_spade(self):
		"""
		At the beginning of the request to the channel, a request to a video is
		made by the browser client.

		All of these video-edge requests include a base 64 encoded string in the
		"data" part of the POST form including analytical data.
		"""
		self.session.post(self.spade_url, headers={
			"Accept": "*/*",
			"Accept-Language": "en-US,en;q=0.5",
			"Accept-Encoding": "gzip, deflate, br",
			"Content-Type": "application/x-www-form-urlencoded; charset=utf-8",
			"Origin": "https://www.twitch.tv",
			"Referer": f"https://www.twitch.tv/{self.channel_name}",
		}, data={
			"data": base64.b64encode(json.dumps({
				"event": "benchmark_template_loaded",
				"properties": {
					"app_version": self.twilight_version,
					"benchmark_server_id": self.session.cookies["server_session_id"],
					"client_time": str(time.time())[:-4],
					"device_id": self.session.cookies["unique_id"],
					"duration": random.randint(500, 5000),
					"url": f"https://www.twitch.tv/{self.channel_name}"
				}
			}).encode("utf-8"))
		}).raise_for_status()

	def set_channel(self, channel_name):
		self.channel_name = channel_name

		# !! Duplicate request !!
		resp = self.session.get(f"https://www.twitch.tv/{channel_name}")

		self.initialize_cdn(resp.text)
		self.initialize_spade()

		resp = self.session.get(f"https://api.twitch.tv/api/channels/{channel_name}/access_token", params={
			"client_id": self.client_id,
			"oauth_token": self.auth,
			"need_https": True,
			"platform": "_",
			"player_type": "site",
			"player_backend": "mediaplayer"
		}, headers={
			"Accept": "application/vnd.twitchtv.v5+json",
			"Origin": "https://www.twitch.tv",
			"Referer": f"https://www.twitch.tv/{channel_name}",
			"X-Requested-With": "XMLHttpRequest"
		})
		resp.raise_for_status()

		token = json.loads(resp.json()["token"])
		self.channel_id = token["channel_id"]

		# Update the channels for the websockets
		self.pubsub.set_channel(self.channel_id)

		self.gql.user(self.unique_id, self.client_id, self.auth)
		self.gql.set_channel(channel_name)

		self.viewer.set_channel(channel_name, self.channel_id)

		points = self.gql.has_available(self.channel_name)

		if points["availableClaim"]:
			log.info("Initial points were detected, claiming now...")
			self.gql.claim(self.channel_id, points["availableClaim"]["id"])

		if not self.gql.is_following(self.channel_name):
			self.gql.follow(self.channel_id)
			log.info(f"Started following @{self.channel_name}")

	def set_user(self, username, auth, unique_id, user_id, client_id):
		self.username = username
		self.auth = auth
		self.unique_id = unique_id
		self.user_id = user_id
		self.client_id = client_id
		
		self.viewer.user(self.username, self.client_id, self.user_id)

	def watch(self):
		return self.viewer.watch(self.channel_name)
