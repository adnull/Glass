import string
import random

def nonce(length):
	return ''.join(random.choice(string.ascii_letters + string.digits) for i in range(length))

def hash(length):
	return ''.join(random.choice(list('abcdef0123456789')) for i in range(length))
