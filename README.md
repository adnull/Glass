# Glass
Twitch livestream viewer that farms channel points on affiliate streamers with a very low overhead (~5MB per account, ~0.1% CPU usage).

## Getting Started

### Prerequisites
* Python 3.6 or above
* At least **one** `.pkl` file including a dictionary of cookies acquired when logging into Twitch on the browser

### Usage
Firstly, make sure that all of your `.pkl` session files are stored in the `/cookies/` folder. Then, run the following command to start the bot.
```python
py main.py
```
From here, the bot will set everything up by itself and immediately start watching if the streamer is online. If the streamer is not online, then it will get all of the bots to watch a different channel, either from the fallback list or from any streamer that any of the bots follow going online.

## Todo
- [ ] Add check if streamer is online when the bot first goes up, then switch channels if it isn't online.
- [x] Make all of the bots follow the streamer.
- [ ] Improve error handling, specifically if a channel goes offline.
- [ ] Improve documentation within the code.
- [x] Add a specific `__main__.py` file, along with more documentation for usage as a library (?).
- [ ] Possibly add a global variable holder, incl. channel IDs with their names etc.
- [ ] Add a global error handler that controls any clients that are watching x channel. So, if channel x goes offline, one client broadcasts to the other ones to shutdown as well, which then wait until they go online again or another fallback streamer goes live.
- [ ] Allow bot to watch x streamer while streamer y is offline.
- [ ] Fully move to aiohttp